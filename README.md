# ROC_withErrorBars

This pyRoot package aims to provide a tool to compute the ROC curve with uncertainties.

## Run
There are many ways to use this tool.

1. You could import the functions themselves, like so:
    ```python
    from roc_w_errors import compute_ROCwErrorBars, compute_AUCwErrorBars
    
    Y_True = np.asarray([...])
    Y_Pred = np.asarray([...])
    Threshold = [...]
    
    Sig, Bkg, Thr = compute_ROCwErrorBars(Y_True,Y_Pred,Threshold)
    auc, auc_err_L, auc_err_H = compute_AUCwErrorBars(Sig,Bkg)
    
    ```

2. You could use the objects

    ```python
    from roc_w_errors import ROC_wErrorBars, AUC_wErrorBars
    
    Y_True = np.asarray([...])
    Y_Pred = np.asarray([...])
    
    roc = ROC_wErrorBars(Y_True,Y_Pred)
    Sig, Bkg, Thr = roc.compute()
    auc, auc_err_L, auc_err_H = roc.get_AUCwErrorBars() 
    
    # or also:
    auc, auc_err_L, auc_err_H = AUC_wErrorBars(Y_True,Y_Pred).compute()
    ```
