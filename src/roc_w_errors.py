#!/usr/bin/env python3
#=============#
# Author: Santiago Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import ROOT as r
# from ROOT import TH1F, TGraphAsymmErrors, Divide
import logging
from scipy.integrate import trapz
# LOGGER = logging.getLogger(__name__)


def _Fill_histo_YTrue(y_true, h_NS, h_NB):
    """
    Fills the given ROOT.THF1 histograms with the True values of the Signal and the Background.
    This is the same as (TP+FN) for Signal, and (TN+FP) for Background.

    # Args:

        y_true: (array) contains true values either {0;1}.

        h_NS: (ROOT.THF1) auxiliary histogram to count true signal entries (TP+FN).

        h_NB: (ROOT.THF1) auxiliary histogram to count true background entries (TN+FP).

    # Returns: None
    """
    for elem in y_true:
        if elem:
            h_NS.Fill(elem)
        elif elem is not None:
            h_NB.Fill(elem)

def _Fill_histo_YPred(y_pred, y_true, thr, h_NSPred, h_NBPred):
    """
    Fills the given ROOT.THF1 histograms with the predicted TP and TN values for Signal and Background respectively.
    At each threshold we evaluate if the prediction is equal to the expected value, if so we fill the corresponding histogram.

    # Args:

        y_true: (array) contains true values either {0;1}.

        y_pred: (array) contains predicted values floats in [0.;1.].

        h_NSPred: (ROOT.THF1) auxiliary histogram to count true predicted signal entries TP.

        h_NBPred: (ROOT.THF1) auxiliary histogram to count true predicted background entries TN.

    # Returns: None
    """
    for j, elem in enumerate(y_pred):
        # -- fill TP from predicted
        if elem > thr and y_true[j]:
            h_NSPred.Fill(elem)
        # -- fill TN from predicted
        elif elem <= thr and not y_true[j]:
            h_NBPred.Fill(elem)

def compute_ROCwErrorBars(y_true,y_pred,thresholds):
    """
    For each threshold it fills two auxiliary histograms to copmute TP and TN.
    It later uses this histograms together with the previously calculated (TP+FN) and (TN+FP) histograms,
    through a Divede method that returns the Signal (Background) efficiency with its uncertainty, respectively.
    As default this uncertainties are computing using a Bayesian approach with a Beta(0.5,0.5) prior.

    # Returns:

        sig_eff_arr: (list) this container holds 3 numpy arrays
            - EffS_arr: the computed efficiency for the Signal.
            - EffS_Errlow_arr: the lower bound of the computed uncertainty.
            - EffS_Errhigh_arr: the upper bound of the computed uncertainty.
        bkg_eff_arr: (list) this container holds 3 numpy arrays
            - EffB_arr: the computed efficiency for the Signal.
            - EffB_Errlow_arr: the lower bound of the computed uncertainty.
            - EffB_Errhigh_arr: the upper bound of the computed uncertainty.
        threshold: (NumpyArray) the thresholds array if it was not provided.
    """
    # -- def outputs
    sig_eff_arr = []
    bkg_eff_arr = []
    # -- def aux containers
    EffS_arr, EffS_Errlow_arr, EffS_Errhigh_arr = [], [], []
    EffB_arr, EffB_Errlow_arr, EffB_Errhigh_arr = [], [], []

    # -- def aux histograms
    h_NSig = r.TH1F('TrueSig','TrueSig',1,0.,1.1)
    h_NBkg = r.TH1F('TrueBkg','TrueBkg',1,0.,1.1)
    h_NPredSig = r.TH1F('PredSig','PredSig',1,0.,1.1)
    h_NPredBkg = r.TH1F('PredBkg','PredBkg',1,0.,1.1)

    # -- def aux graph error var to compute the errors from histograms
    eff_Sig = r.TGraphAsymmErrors()
    eff_Bkg = r.TGraphAsymmErrors()

    # -- fill denominators
    _Fill_histo_YTrue(y_true=y_true, h_NS=h_NSig, h_NB=h_NBkg)

    logging.debug(f'DenS len: {h_NSig.GetEntries()}')
    logging.debug(f'DenB len: {h_NBkg.GetEntries()}')

    for thr in thresholds:
        # -- clean histograms
        h_NPredSig.Reset()
        h_NPredBkg.Reset()
        _Fill_histo_YPred(
                y_pred=y_pred,
                y_true=y_true,
                thr=thr,
                h_NBPred=h_NPredBkg,
                h_NSPred=h_NPredSig
                        )
        # -- checking --
        logging.debug(f'Spred len: {h_NPredSig.GetEntries()}')
        logging.debug(f'Bpred len:  {h_NPredBkg.GetEntries()}')

        # -- compute the eff with errors
        eff_Sig.Divide(h_NPredSig, h_NSig, 'cl=0.683 b(0.5,0.5) mode')
        eff_Bkg.Divide(h_NPredBkg, h_NBkg, 'cl=0.683 b(0.5,0.5) mode')

        # -- append to containers
        # -- the coord x of the .Divide method is the bin center so we don't need it
        # -- the eff is the y coord of the .Dived method
        # -- NOTE: we only need the first element since there is only 1 bin.
        EffS_arr.append(eff_Sig.GetPointY(0))
        EffS_Errlow_arr.append(eff_Sig.GetErrorYlow(0))
        EffS_Errhigh_arr.append(eff_Sig.GetErrorYhigh(0))

        EffB_arr.append(eff_Bkg.GetPointY(0))
        EffB_Errlow_arr.append(eff_Bkg.GetErrorYlow(0))
        EffB_Errhigh_arr.append(eff_Bkg.GetErrorYhigh(0))

    sig_eff_arr = [np.asarray(EffS_arr),
                   np.asarray(EffS_Errlow_arr),
                   np.asarray(EffS_Errhigh_arr)]
    bkg_eff_arr = [np.asarray(EffB_arr),
                   np.asarray(EffB_Errlow_arr),
                   np.asarray(EffB_Errhigh_arr)]

    return sig_eff_arr, bkg_eff_arr, thresholds

def compute_AUCwErrorBars(sig_eff_arr, bkg_eff_arr, precision=5):
    """
    It computes the AUC of the curve defined by (x,y)=(tpr,fpr) using the trapezoidal rule for integration.
    To compute the upper(lower) uncertainty it integrates the curve defined by (xErr_upper(lower),yErr_upper(lower)).

    # Args::
        sig_eff_arr: (list), container of arrays with the signal efficiency (tpr) and its upper and lower uncertainties.
        bkg_eff_arr: (list), container of arrays with the background efficiency (fpr) and its upper and lower uncertainties.
        precision: (int), number of significant numbers.

    # Returns::
        auc: (float), the integral under the curve defined by (tpr,fpr).
        LowErr: (float), lower bound uncertainty of the estimated AUC.
        HighErr: (float), upper bound uncertainty of the estimated AUC.
    """
    x, xEl, xEh = sig_eff_arr
    y, yEl, yEh = bkg_eff_arr

    # -- Set new vars to compute the upper and lower auc.
    x_low = x - xEl
    y_low = y - yEl
    x_high = x + xEh
    y_high = y + yEh

    # -- Integrate the curves using trapezoidal rule.
    auc = abs(trapz(y,x,axis=0))
    auc_L = trapz(y_low,x_low)
    auc_H = trapz(y_high,x_high)

    # -- Compute errors relative to auc magnitud.
    LowErr = auc - abs(auc_L)
    HighErr = abs(auc_H) - auc

    # -- round results to given precision.
    auc = np.round(auc,precision)
    LowErr = np.round(LowErr,precision)
    HighErr = np.round(HighErr,precision)

    return auc, LowErr, HighErr


class ROC_wErrorBars(object):
    def __init__(self,y_true,y_pred,threshold=None,auc_precision=None):
        self.Y_True = y_true
        self.Y_Pred = y_pred
        if threshold is None:
            self.threshold = np.linspace(start=0.,stop=1.,num=50)
        else:
            assert all(threshold) <= 1.
            self.threshold = threshold
        if auc_precision is None:
            self.precision = 5
        else:
            assert isinstance(auc_precision,int)
            self.precision = auc_precision

    def compute(self):
        """wrapper to call `compute_ROCwErrorBars`"""
        call = compute_ROCwErrorBars(
                y_true=self.Y_True,
                y_pred=self.Y_Pred,
                thresholds=self.threshold
                )
        return call

    def get_AUCwErrorBars(self):
        """wrapper to call `compute_AUCwErrorBars`"""
        S_Eff, B_Eff, _ = self.compute()
        call = compute_AUCwErrorBars(
                sig_eff_arr = S_Eff,
                bkg_eff_arr = B_Eff,
                precision = self.precision
                )
        return call

class AUC_wErrorBars(ROC_wErrorBars):
    def __init__(self,y_true, y_pred, precision=None):
        super().__init__(y_true,y_pred)
        if precision is None:
            self.precision = 5
        else:
            self.precision = precision

    def compute(self):
        S_Eff, B_Eff, _ = ROC_wErrorBars(self.Y_True,self.Y_Pred).compute()
        call = compute_AUCwErrorBars(
                sig_eff_arr = S_Eff,
                bkg_eff_arr = B_Eff,
                precision = self.precision
                )
        return call
